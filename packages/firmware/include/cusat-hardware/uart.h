#pragma once

#include <stdint.h>
#include <stddef.h>

void cusat_UART_start();

void cusat_UART_write(const void *const data, const size_t length);

void cusat_UART_read(void *const data, const size_t length);

void cusat_UART_stop();