#pragma once

#include <stddef.h>
#include <stdint.h>

void cusat_I2C_start();

void cusat_I2C_write(const uint8_t address, const void *const data, const size_t length);

void cusat_I2C_read(const uint8_t address, void *const data, const size_t length);

void cusat_I2C_stop();