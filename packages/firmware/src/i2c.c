#include <cusat-hardware/i2c.h>

#include <stdio.h>

int I2C_start(unsigned int i2cBusSpeed_Hz, unsigned int i2cTransferTimeout);
int I2C_write(unsigned int slaveAddress, unsigned char *data, unsigned int size);
int I2C_read(unsigned int slaveAddress, unsigned char *data, unsigned int size);
void I2C_stop();

void cusat_I2C_start()
{
#ifdef at91sam9g20
    printf("Starting ISIS I2C interface.\n");
    I2C_start(100e3, 5000);
#else
    printf("Starting (simulated) I2C interface!\n");
#endif
}

void cusat_I2C_write(const uint8_t address, const void *const data, const size_t length)
{
#ifdef at91sam9g20
    I2C_write(address, data, length);
#endif
}

void cusat_I2C_read(const uint8_t address, void *const data, const size_t length)
{
#ifdef at91sam9g20
    I2C_read(address, data, length);
#endif
}

void cusat_I2C_stop()
{
#ifdef at91sam9g20
    printf("Stopping ISIS I2C interface.\n");
    I2C_stop();
#else
    printf("Stopping (simulated) I2C interface!\n");
#endif
}