#include <cusat-hardware/i2c.h>

#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include <stdbool.h>
#include <stdio.h>

const uint8_t SLAVE_ADDRESS = 4;

void Start()
{
    cusat_I2C_start();
}

void sendData()
{
    char data[] = "x is _";
    data[5] = 42;
    const size_t length = sizeof(data) / sizeof(data[0]);
    cusat_I2C_write(SLAVE_ADDRESS, (const void *const)data, length - 1);
}

void receiveData()
{
    // 7 bytes + null = 8
    // x was _
    char data[8] = {};
    const size_t length = sizeof(data) / sizeof(data[0]);
    cusat_I2C_read(SLAVE_ADDRESS, (void *const)data, length - 1);
    data[7] = 0;

    uint8_t byte = data[6];
    data[6] = 0;

    printf("receiving data: ");
    printf(data);
    printf("%u", byte);
    printf("\r\n");
}

void Update()
{
    sendData();
    vTaskDelay(500 / portTICK_RATE_MS);
    receiveData();
}